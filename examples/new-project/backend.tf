# [terraform-project]/backend.tf

terraform {
  backend "gcs" {
    bucket = "dns-zones-a1ce7e00-terraform-state"
    prefix = "example-com-dns-zone-iac"
  }
}
