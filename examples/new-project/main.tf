# [terraform-project]/main.tf

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.13"
    }
  }
  required_version = ">= 1.0.4"
}

# Providers are defined in providers.tf

# Module with DNS Hosted Zone Configuration
# All DNS records are defined in terraform.tfvars.json
module "gcp_cloud_dns_managed_zone" {
  source = "git::https://gitlab.com/gitlab-com/sandbox-cloud/terraform-modules/gcp/gcp-cloud-dns-managed-zone-module.git?ref=2.5.10"

  a_records            = var.a_records
  cname_records        = var.cname_records
  dns_zone_description = var.dns_zone_description
  dns_zone_fqdn        = var.dns_zone_fqdn
  dns_zone_name        = var.dns_zone_name
  dnssec_enabled       = var.dnssec_enabled
  gcp_project          = var.gcp_project
  root_a_record        = var.root_a_record
  root_mx_records      = var.root_mx_records
  subdomain_mx_records = var.subdomain_mx_records
  subzones             = var.subzones
  txt_records          = var.txt_records
}
