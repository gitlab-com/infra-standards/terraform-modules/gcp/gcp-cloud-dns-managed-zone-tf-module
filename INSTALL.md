# Installation Instructions

These instructions will help you get started with creating a new managed zone using the example provided with this module.

## GCP Project

### GCP Cloud Storage Bucket

1. Open the GCP Cloud Console and select the appropriate project (Ex. `dns-zones-########`).

1. Navigate to `Cloud Storage > Browser`.

1. Create a new bucket or verify that an existing bucket exists for Terraform state. (Ex. `dns-zones-########-terraform-state`).

    > The name of the bucket must be globally unique with GCP, so it is recommended to use the GCP project name as a prefix.

### GCP Cloud DNS Service

1. Navigate to `Network Services > Cloud DNS`.

1. Enable the `Cloud DNS API` service if it has not already been enabled. After it is enabled, you will see a list of DNS zones.

1. Take note of the names of the existing DNS zones to ensure consistency when assigning a name to the DNS zone in this environment.

### GCP IAM Service Account

1. Navigate to `IAM & Admin > Service Accounts`.

1. Create a new service account for Terraform.

    > If using GitLab CI, it is best practice for the service account to include `terraform-ci` or `tf-ci` in the name. At GitLab, we use the alphadash name (path) of the GitLab project with a `terraform-ci` or `tf-ci` suffix depending on length limits. (Ex. `example-com-dns-tf-ci`)
    >
    > If you run into 30 character length limits, it's best to remove characters from the end of the domain name to ensure suffix consistency so we can accommodate different TLDs (ex. `.com` and `.net`) for the same long domain name (Ex. `reallylongdomainname-com-dns-tf-ci` => `reallylongdomain-com-dns-tf-ci`)

1. Add the following pre-defined roles to the service account.
    * `DNS Administrator`
    * `Storage Object Admin`
    * `Service Account User`
    * `Browser`

1. Create a JSON key for the service account. Open the downloaded file in your text editor and save the contents to a new record in your password manager (ex. 1Password). For GitLab Infrastructure Shared Services, this should be stored in the `Infra Service - DNS` 1Password vault.

## GitLab Project

1. Create a new GitLab project in your desired group/namespace where your Terraform source code will reside. At GitLab, this will be on the [ops.gitlab.net](https://ops.gitlab.net) or [gitops.gitlabsandbox.cloud](https://gitops.gitlabsandbox.cloud) instance. For internal GitLab projects, we have project templates defined with this module's example.
    > **For security reasons, ensure that your project is `Private` and not `Public` or `Internal`.**

### CI Variable for Service Account

1. Navigate to `Settings > CI/CD` and expand the `Variables` section.

1. Create a new CI variable.
    * Key: `GOOGLE_APPLICATION_CREDENTIALS` (cannot be different)
    * Value: (copy and paste JSON from password manager)
    * Type: `File`
    * Environment Scope: `All (Default)`
    * Flags - Protect Variable: (unchecked)
    * Flags - Mask Variable: (unchecked)

### Branch for Configuration Commits

1. Navigate to `Issues` for this project in the left sidebar.

1. Create a new issue titled `Initial configuration of Terraform environment and variables` or similar.

1. Create a new merge request from the issue.

1. Open the Web IDE (or clone to your local machine and check out the branch).

1. Copy the files in `examples/new-project` from the module to the Git repository. If you are using a project template, these may already exist in the project.

1. Rename the `.gitignore.example` to `.gitignore`.

### Update the Terraform Variables

1. Edit the `backend.tf` file.
    * Replace the `bucket` value with the bucket name you configured above. (Ex. `dns-zones-########-terraform-state`)
    * Replace the `prefix` value with your GitLab project path. (Ex. `example-com-dns-tf`)

1. Open the `terraform.tfvars.json.example` file. Take a few moments to study the syntax and examples.

1. Edit the `terraform.tfvars.json` file.
    * Replace the `dns_zone_name` value with the alphadash name of the domain name and a suffix. (Ex. `example-com-zone`)
    * Replace the `dns_zone_fqdn` value with FQDN of the domain name with a trailing period. (Ex. `example.com.`)
    * Use the `terraform.tfvars.json.example` file examples for adding records to any applicable arrays. You can always make changes later.

1. Commit your changes to the branch. (Ex. `Update Terraform configuration with environment values`)

### Using CI Jobs for Terraform Commands

1. When you make changes in a merge request branch, a CI pipeline will run automatically to perform the `terraform init`, `terraform validate`, and `terraform plan` commands.

1. Check the status of the `Validate` job in the `Dry Run` stage.
    * Make any changes to the code if any errors are displayed.

1. When the `Validate` job succeeds, proceed with merging the merge request.

1. Navigate to the `CI Pipelines` for this GitLab project and locate the latest pipeline. This is usually provided as a link after you merge the merge request.

1. **Option A:** If the DNS managed zone already exists in the GCP project, run the `Import Zone` job in the `Deploy` stage.
    > The managed zone is usually automatically created when you purchase or transfer a domain name. This will run the `terraform import` command to bring the existing DNS managed zone into the Terraform state and update the existing GCP configuration based on the Terraform resource configuration.

1. **Option B:** If the DNS managed zone does not exist yet, run the `Create Zone` job in the `Deploy` stage.

1. After the zone has successfully been created or imported, you can run the `Everything` job in the `Deploy` stage to create all of the DNS records.
    > If a record already exists with the same key/name, you may need to temporarily delete the record for Terraform to be able to recreate it. It is important to verify that your configuration in Terraform is the same as the pre-existing GCP Console configuration. You should also screenshot the existing state before deleting it to be safe.

1. Verify the outputs in the `Everything` CI job that all of your DNS records have been created.
    * Make any changes to the code if any errors are displayed. You will need to create a new issue/MR to make changes before merging them into the `main` branch where you can run `terraform apply` commands using the `Deploy` stage CI jobs.

See the `README.md` for day-to-day DNS record management instructions.
